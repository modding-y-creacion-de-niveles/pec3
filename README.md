# PEC2
Proyecto de la PEC 3 del curso de Modding y creación de niveles.


## Como Jugar
El juego inicia con el personaje en una ubicación inicial en el escenario, donde estaremos rodeados por muros y encontraremos cajas las cuales debemos de ubicar sobre una base con el símbolo de la misma. Colocando todas las cajas pasaremos de nivel.

El juego consiste de 10 niveles, al final nos enviara a un menur de Fin de juego, donde podemos reiniciar el juego o salir de este.

## Archivos del juego

Se pueden encontrar varias carpetas. Las principales serian las siguientes:

Prefabs: encontramos una carpenta que contienes todos los prefabs del juego desde el personaje que controlamos las cajas, las bases de las cajas y lor muros


Scenes: 
 - GameOver: se encuentra la interfaz de GameOver
 - Escenario: a primera vista pareciera un escenario vacio pero al iniciar el juego, se generará el mapa según las imagenes predefinidas.

Sprites:
 - Carpeta niveles: contiene las imagenes que se usaran en el editor para generar los mapas.
 - Carpeta Player: sprite para una implementación posterior de la animación del personaje.
 - También se encuentran los sprites de las cajas, sus bases y los muros.

Scripts:
 - Base_Caja: en este codigo da a conocer si una caja esta sobre una base.
 - BtnOpciones: codigo para cambiar de escena al Escenario
 - ColorToPrefab: esta el codigo que especifica el color y el prefab usado.
 - ExitGame: codigo para salir del juego
 - LevelGenerator: codigo donde generara e eliminará el nivel con las imagenes .png, generandolo todo en la misma escena.
 - PlayerController: codigo con el cual controlaremos al jugador por cuadro, solo de forma horizontal y vertical; omitiendo el movimiento en diagonal. También encontraremos la logica para que no traspace los mujos y con cajas adjacentes a muros.
