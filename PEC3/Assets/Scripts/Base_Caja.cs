using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base_Caja : MonoBehaviour
{

    public int pulsado;

    // Start is called before the first frame update
    void Start()
    {
        pulsado = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Caja"))
        {
            pulsado = 1;
        }
        else
        {
            pulsado = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Caja"))
        {
            pulsado = -1;
        }
    }
}
