using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGenerator : MonoBehaviour
{
    public Texture2D[] map;
    public ColorToPrefab[] colorMappings;
    private int suma, l;
    public GameObject[] objCajas, objJugador, objMuros;
    public Base_Caja[] base_caja;

    private void Start()
    {
        l = 0;
        GenerateLevel(l);
        suma = 0;
        base_caja = FindObjectsOfType<Base_Caja>();

        StartCoroutine("Coroutine", 0);
        
    }

    private void Update()
    {
        if (suma == base_caja.Length)
        {
            Debug.Log("Objetos Eliminados");
            Invoke("eliminarObjetos", 1);
            Debug.Log("Suma TOTAL: " + suma);
        }
    }

    IEnumerator Coroutine(int l)
    {
        suma = 0;
        for (int i = 0; i < base_caja.Length; i++)
        {
            suma = base_caja[i].pulsado + suma;
            

            if (suma == base_caja.Length)
            {
                l = l + 1;

                if (l==3)
                {
                    Debug.Log("JUEGO TERMINADO");
                    SceneManager.LoadScene("GameOver");
                }
                Debug.Log("Se va a generar Mapa, L: " + l);
                yield return new WaitForSeconds(4);
                suma = 0;
                yield return new WaitForSeconds(2);
                GenerateLevel(l);
                yield return new WaitForSeconds(1);
                base_caja = FindObjectsOfType<Base_Caja>();
                Debug.Log("Cambia de escena");
            }
        }

        yield return new WaitForSeconds(1);
        StartCoroutine("Coroutine", l);        
    }

    void GenerateLevel(int l)
    {
        for (int x = 0; x < map[l].width; x++)
        {
            for (int y = 0; y < map[l].height; y++)
            {
                GenerateTile(x, y, l);
            }
        }

    }

    void GenerateTile(int x, int y, int l)
    {
        Color pixelColor = map[l].GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            return;
        }

        foreach (ColorToPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
            }
        }
    }

    void guardarObjetos()
    {
        objCajas = GameObject.FindGameObjectsWithTag("Caja");
        objJugador = GameObject.FindGameObjectsWithTag("Player");
        objMuros = GameObject.FindGameObjectsWithTag("Muro");
    }

    void eliminarObjetos()
    {
        for (int x = 0; x < GameObject.FindGameObjectsWithTag("Caja").Length; x++)
        {
            Destroy(GameObject.FindWithTag("Caja"));
            Destroy(GameObject.FindWithTag("Base_Caja"));
        }


        for (int x = 0; x < GameObject.FindGameObjectsWithTag("Player").Length; x++)
        {
            Destroy(GameObject.FindWithTag("Player"));
        }

        for (int x = 0; x < GameObject.FindGameObjectsWithTag("Muro").Length; x++)
        {
            Destroy(GameObject.FindWithTag("Muro"));
        }
    }
}
