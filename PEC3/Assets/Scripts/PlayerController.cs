using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float velocidadMoviemiento;
    private Vector2 puntoMoviemiento;
    [SerializeField] private Vector2 offSetPuntoMovimiento, offSet2;
    [SerializeField] private LayerMask muros, caja;
    [SerializeField] private float radioCirculo;
    private bool moviendo = false;
    private Vector2 input;

    private void Start()
    {
        puntoMoviemiento = transform.position;
    }

    private void Update()
    {
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");

        if (moviendo)
        {
            transform.position = Vector2.MoveTowards(transform.position, puntoMoviemiento, velocidadMoviemiento * Time.deltaTime);

            if (Vector2.Distance(transform.position, puntoMoviemiento) == 0)
            {
                moviendo = false;
            }
        }

        if ((input.x != 0 ^ input.y != 0) && !moviendo)
        {
            Vector2 puntoEvaluar = new Vector2(transform.position.x, transform.position.y) + offSetPuntoMovimiento + input;
            Vector2 puntoEvaluar2 = new Vector2(transform.position.x, transform.position.y) + offSet2 + input*2;

            if (!Physics2D.OverlapCircle(puntoEvaluar, radioCirculo, muros))
            {
                moviendo = true;
                puntoMoviemiento += input;
            }

            if ((Physics2D.OverlapCircle(puntoEvaluar, radioCirculo, caja) && Physics2D.OverlapCircle(puntoEvaluar2, radioCirculo, muros)) || (Physics2D.OverlapCircle(puntoEvaluar, radioCirculo, caja) && Physics2D.OverlapCircle(puntoEvaluar2, radioCirculo, caja)))
            {
                moviendo = false;
                puntoMoviemiento -= input;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(puntoMoviemiento + offSetPuntoMovimiento, radioCirculo);
        Gizmos.DrawWireSphere(puntoMoviemiento + offSet2 + input, radioCirculo);
    }
}
